package com.privalia.jmgarzo.privaliachallenge;

import android.content.ContentValues;
import android.database.Cursor;

import com.privalia.jmgarzo.privaliachallenge.data.PrivaliaContract;

import java.util.Map;
import java.util.Set;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;

/**
 * Created by jmgar on 28/01/2018.
 */

public class TestUtils {

    public static final int BULK_INSERT_MOVIE_RECORDS_TO_INSERT = 10;

    static ContentValues createTestMovieContentValues() {
        ContentValues testMovieValues = new ContentValues();

        testMovieValues.put(PrivaliaContract.MovieEntry.POSTER_PATH, "/jjBgi2r5cRt36xF6iNUEhzscEcb.jpg");
        testMovieValues.put(PrivaliaContract.MovieEntry.ADULT, "false");
        testMovieValues.put(PrivaliaContract.MovieEntry.OVERVIEW, "Twenty-two years after the events of Jurassic Park, Isla Nublar now features a fully functioning dinosaur theme park, Jurassic World, as originally envisioned by John Hammond.");
        testMovieValues.put(PrivaliaContract.MovieEntry.RELEASE_DATE, "2015-06-09");
        testMovieValues.put(PrivaliaContract.MovieEntry.MOVIE_WEB_ID, "135397");
        testMovieValues.put(PrivaliaContract.MovieEntry.ORIGINAL_TITLE, "Jurassic World");
        testMovieValues.put(PrivaliaContract.MovieEntry.ORIGINAL_LANGUAGE, "en");
        testMovieValues.put(PrivaliaContract.MovieEntry.TITLE, "Jurassic World");
        testMovieValues.put(PrivaliaContract.MovieEntry.BACKDROP_PATH, "/dkMD5qlogeRMiEixC4YNPUvax2T.jpg");
        testMovieValues.put(PrivaliaContract.MovieEntry.TITLE, "Jurassic World");
        testMovieValues.put(PrivaliaContract.MovieEntry.POPULARITY, 68.465); //68.465163
        testMovieValues.put(PrivaliaContract.MovieEntry.VOTE_COUNT, 6456);
        testMovieValues.put(PrivaliaContract.MovieEntry.VIDEO, "false");
        testMovieValues.put(PrivaliaContract.MovieEntry.VOTE_AVERAGE, 6.5);
        testMovieValues.put(PrivaliaContract.MovieEntry.REGISTRY_TYPE, PrivaliaContract.MOST_POPULAR_REGISTRY_TYPE);

        return testMovieValues;
    }

    static void validateThenCloseCursor(String error, Cursor valueCursor, ContentValues expectedValues) {
        assertNotNull("This cursor is null. Did you make sure to register your ContentProvider in the manifest?",
                valueCursor);
        assertTrue("Empty cursor returned. " + error, valueCursor.moveToFirst());
        validateCurrentRecord(error, valueCursor, expectedValues);
        valueCursor.close();
    }

    static void validateCurrentRecord(String error, Cursor valueCursor, ContentValues expectedValues) {
        Set<Map.Entry<String, Object>> valueSet = expectedValues.valueSet();

        for (Map.Entry<String, Object> entry : valueSet) {
            String columnName = entry.getKey();
            int index = valueCursor.getColumnIndex(columnName);

            /* Test to see if the column is contained within the cursor */
            String columnNotFoundError = "Column '" + columnName + "' not found. " + error;
            assertFalse(columnNotFoundError, index == -1);

            /* Test to see if the expected value equals the actual value (from the Cursor) */
            String expectedValue = entry.getValue().toString();
            String actualValue = valueCursor.getString(index);


            String valuesDontMatchError = "Actual value '" + actualValue
                    + "' did not match the expected value '" + expectedValue + "'. "
                    + error;

            assertEquals(valuesDontMatchError,
                    expectedValue,
                    actualValue);
        }
    }

    static ContentValues[] createBulkInsertTestMovieValues(){

        ContentValues[] bulkTestMovieValues = new ContentValues[BULK_INSERT_MOVIE_RECORDS_TO_INSERT];

        for (int i=0; i<BULK_INSERT_MOVIE_RECORDS_TO_INSERT;i++ ){

            ContentValues testMovieValues = new ContentValues();

            testMovieValues.put(PrivaliaContract.MovieEntry.POSTER_PATH, "/jjBgi2r5cRt36xF6iNUEhzscEcb" + i +".jpg");
            testMovieValues.put(PrivaliaContract.MovieEntry.ADULT, "false");
            testMovieValues.put(PrivaliaContract.MovieEntry.OVERVIEW, i +"Twenty-two years after the events of Jurassic Park, Isla Nublar now features a fully functioning dinosaur theme park, Jurassic World, as originally envisioned by John Hammond.");
            testMovieValues.put(PrivaliaContract.MovieEntry.RELEASE_DATE, "2015-06-09");
            testMovieValues.put(PrivaliaContract.MovieEntry.MOVIE_WEB_ID, i+"_135397");
            testMovieValues.put(PrivaliaContract.MovieEntry.ORIGINAL_TITLE, i + "_Jurassic World");
            testMovieValues.put(PrivaliaContract.MovieEntry.ORIGINAL_LANGUAGE, i+"_en");
            testMovieValues.put(PrivaliaContract.MovieEntry.TITLE, i+"_Jurassic World");
            testMovieValues.put(PrivaliaContract.MovieEntry.BACKDROP_PATH, i+"_/dkMD5qlogeRMiEixC4YNPUvax2T.jpg");
            testMovieValues.put(PrivaliaContract.MovieEntry.TITLE, i+"_Jurassic World");
            testMovieValues.put(PrivaliaContract.MovieEntry.POPULARITY, 68.465 + i); //68.465163
            testMovieValues.put(PrivaliaContract.MovieEntry.VOTE_COUNT, 6456 +i);
            testMovieValues.put(PrivaliaContract.MovieEntry.VIDEO, "false");
            testMovieValues.put(PrivaliaContract.MovieEntry.VOTE_AVERAGE, 6.5 + i);
            testMovieValues.put(PrivaliaContract.MovieEntry.REGISTRY_TYPE, PrivaliaContract.MOST_POPULAR_REGISTRY_TYPE);

            bulkTestMovieValues[i] = testMovieValues;
        }

        return bulkTestMovieValues;
    }
}



