package com.privalia.jmgarzo.privaliachallenge;

import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.ProviderInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.privalia.jmgarzo.privaliachallenge.data.PrivaliaContract;
import com.privalia.jmgarzo.privaliachallenge.data.PrivaliaDBHelper;
import com.privalia.jmgarzo.privaliachallenge.data.PrivaliaProvider;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static junit.framework.Assert.fail;

/**
 * Created by jmgar on 28/01/2018.
 */

@RunWith(AndroidJUnit4.class)
public class TestMoviesProvider {


    private final Context mContext = InstrumentationRegistry.getTargetContext();
    private int movieId;

    @Before
    public void setUp() {
        deleteAllRecords();
    }

    @Test
    public void testProviderRegistry() {

        String packageName = mContext.getPackageName();
        String PrivaliaProviderClassName = PrivaliaProvider.class.getName();
        ComponentName componentName = new ComponentName(packageName, PrivaliaProviderClassName);

        try {

            PackageManager pm = mContext.getPackageManager();

            /* The ProviderInfo will contain the authority, which is what we want to test */
            ProviderInfo providerInfo = pm.getProviderInfo(componentName, 0);
            String actualAuthority = providerInfo.authority;
            String expectedAuthority = PrivaliaContract.CONTENT_AUTHORITY;

            /* Make sure that the registered authority matches the authority from the Contract */
            String incorrectAuthority =
                    "Error: PopularMoviesProvider registered with authority: " + actualAuthority +
                            " instead of expected authority: " + expectedAuthority;
            assertEquals(incorrectAuthority,
                    actualAuthority,
                    expectedAuthority);

        } catch (PackageManager.NameNotFoundException e) {
            String providerNotRegisteredAtAll =
                    "Error: WeatherProvider not registered at " + mContext.getPackageName();
            fail(providerNotRegisteredAtAll);
        }

    }

    @Test
    public void testBasicMovieQuery(){
        PrivaliaDBHelper dbHelper = new PrivaliaDBHelper(mContext);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        ContentValues testMovieValues = TestUtils.createTestMovieContentValues();

        long movieRowId = db.insert(PrivaliaContract.MovieEntry.TABLE_NAME,null,testMovieValues);

        String insertFailed = "Unable to insert into the database";
        assertTrue(insertFailed,movieRowId != -1);

        db.close();

        Cursor movieCursor = mContext.getContentResolver().query(
                PrivaliaContract.MovieEntry.CONTENT_URI,
                null,
                null,
                null,
                null
        );

        TestUtils.validateThenCloseCursor("testBasicMovieQuery",
                movieCursor,
                testMovieValues);

        movieId = movieCursor.getColumnIndex(PrivaliaContract.MovieEntry._ID);



    }

    @Test
    public void testBulkMovieInsert(){
        ContentValues[] bulkInsertContentValues = TestUtils.createBulkInsertTestMovieValues();

        int insertCount = mContext.getContentResolver().bulkInsert(
                PrivaliaContract.MovieEntry.CONTENT_URI,
                bulkInsertContentValues);

        String expectedAndActualInsertedRecordCountDoNotMatch =
                "Number of expected records inserted does not match actual inserted record count";
        assertEquals(expectedAndActualInsertedRecordCountDoNotMatch,
                insertCount,
                TestUtils.BULK_INSERT_MOVIE_RECORDS_TO_INSERT);


        Cursor cursor = mContext.getContentResolver().query(
                PrivaliaContract.MovieEntry.CONTENT_URI,
                null,
                null,
                null,
                PrivaliaContract.MovieEntry._ID + " ASC");

        assertEquals(cursor.getCount(), TestUtils.BULK_INSERT_MOVIE_RECORDS_TO_INSERT);

        cursor.moveToFirst();
        for (int i = 0; i < TestUtils.BULK_INSERT_MOVIE_RECORDS_TO_INSERT; i++, cursor.moveToNext()) {
            TestUtils.validateCurrentRecord(
                    "testBulkInsert. Error validating MovieEntry " + i,
                    cursor,
                    bulkInsertContentValues[i]);
        }

        /* Always close the Cursor! */
        cursor.close();

    }




    private void deleteAllRecords() {
        PrivaliaDBHelper helper = new PrivaliaDBHelper(InstrumentationRegistry.getTargetContext());
        SQLiteDatabase database = helper.getWritableDatabase();

        database.delete(PrivaliaContract.MovieEntry.TABLE_NAME, null, null);

        database.close();
    }
}
