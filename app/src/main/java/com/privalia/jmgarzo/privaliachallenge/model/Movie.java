package com.privalia.jmgarzo.privaliachallenge.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.privalia.jmgarzo.privaliachallenge.utilities.DatabaseUtil;
import com.privalia.jmgarzo.privaliachallenge.data.PrivaliaContract;

/**
 * Created by jmgar on 24/01/2018.
 */

public class Movie implements Parcelable {

    private String LOG_TAG = Movie.class.getSimpleName();


    private int id;
    private String posterPath;
    private boolean adult;
    private String overview;
    private String releaseDate;
    private String movieWebId;
    private String originalTitle;
    private String originalLanguage;
    private String title;
    private String backdropPath;
    private double popularity;
    private int voteCount;
    private boolean video;
    private double voteAverage;
    private String registryType;
    private String timestamp;

    public Movie() {
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPosterPath() {
        return posterPath;
    }

    public void setPosterPath(String posterPath) {
        this.posterPath = posterPath;
    }

    public boolean isAdult() {
        return adult;
    }

    public void setAdult(boolean adult) {
        this.adult = adult;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getMovieWebId() {
        return movieWebId;
    }

    public void setMovieWebId(String movieWebId) {
        this.movieWebId = movieWebId;
    }

    public String getOriginalTitle() {
        return originalTitle;
    }

    public void setOriginalTitle(String originalTitle) {
        this.originalTitle = originalTitle;
    }

    public String getOriginalLanguage() {
        return originalLanguage;
    }

    public void setOriginalLanguage(String originalLanguage) {
        this.originalLanguage = originalLanguage;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBackdropPath() {
        return backdropPath;
    }

    public void setBackdropPath(String backdropPath) {
        this.backdropPath = backdropPath;
    }

    public double getPopularity() {
        return popularity;
    }

    public void setPopularity(double popularity) {
        this.popularity = popularity;
    }

    public int getVoteCount() {
        return voteCount;
    }

    public void setVoteCount(int voteCount) {
        this.voteCount = voteCount;
    }

    public boolean isVideo() {
        return video;
    }

    public void setVideo(boolean video) {
        this.video = video;
    }

    public double getVoteAverage() {
        return voteAverage;
    }

    public void setVoteAverage(double voteAverage) {
        this.voteAverage = voteAverage;
    }

    public String getRegistryType() {
        return registryType;
    }

    public void setRegistryType(String registryType) {
        this.registryType = registryType;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * Construct a new Movie from a cursor's first row.
     *
     * @param cursor
     */
    public Movie(Cursor cursor) {
        if (cursor != null && cursor.moveToFirst()) {
            if (cursor.getCount() > 1) {
                Log.w(LOG_TAG, "Cursor have more than one rows");
            }
            cursorToMovie(cursor);
        }
    }


    /**
     * Movie's constructor from a cursor and a position
     * @param cursor
     * @param position
     */
    public Movie(Cursor cursor, int position) {
        if (cursor != null && cursor.moveToPosition(position)) {
            cursorToMovie(cursor);
        }
    }

    private void cursorToMovie(Cursor cursor) {
        id = cursor.getInt(DatabaseUtil.COL_MOVIE_ID);
        posterPath = cursor.getString(DatabaseUtil.COL_MOVIE_POSTER_PATH);
        adult = Boolean.valueOf(cursor.getString(DatabaseUtil.COL_MOVIE_ADULT));
        overview = cursor.getString(DatabaseUtil.COL_MOVIE_OVERVIEW);
        releaseDate = cursor.getString(DatabaseUtil.COL_MOVIE_RELEASE_DATE);
        movieWebId = cursor.getString(DatabaseUtil.COL_MOVIE_MOVIE_WEB_ID);
        originalTitle = cursor.getString(DatabaseUtil.COL_MOVIE_ORIGINAL_TITLE);
        originalLanguage = cursor.getString(DatabaseUtil.COL_MOVIE_ORIGINAL_LANGUAGE);
        title = cursor.getString(DatabaseUtil.COL_MOVIE_TITLE);
        backdropPath = cursor.getString(DatabaseUtil.COL_MOVIE_BACKDROP_PATH);
        popularity = cursor.getDouble(DatabaseUtil.COL_MOVIE_POPULARITY);
        voteCount = cursor.getInt(DatabaseUtil.COL_MOVIE_VOTE_COUNT);
        video = Boolean.valueOf(cursor.getString(DatabaseUtil.COL_MOVIE_VIDEO));
        voteAverage = cursor.getDouble(DatabaseUtil.COL_MOVIE_VOTE_AVERAGE);
        registryType = cursor.getString(DatabaseUtil.COL_MOVIE_REGISTRY_TYPE);
        timestamp = cursor.getString(DatabaseUtil.COL_MOVIE_TIMESTAMP);
    }

    /**
     * @return a movie without _id and timestamp to make an insert
     */
    public ContentValues getContentValues() {
        ContentValues contentValues = new ContentValues();

        contentValues.put(PrivaliaContract.MovieEntry.POSTER_PATH, getPosterPath());
        contentValues.put(PrivaliaContract.MovieEntry.ADULT, Boolean.toString(isAdult()));
        contentValues.put(PrivaliaContract.MovieEntry.OVERVIEW, getOverview());
        contentValues.put(PrivaliaContract.MovieEntry.RELEASE_DATE, getReleaseDate());
        contentValues.put(PrivaliaContract.MovieEntry.MOVIE_WEB_ID, getMovieWebId());
        contentValues.put(PrivaliaContract.MovieEntry.ORIGINAL_TITLE, getOriginalTitle());
        contentValues.put(PrivaliaContract.MovieEntry.ORIGINAL_LANGUAGE, getOriginalLanguage());
        contentValues.put(PrivaliaContract.MovieEntry.TITLE, getTitle());
        contentValues.put(PrivaliaContract.MovieEntry.BACKDROP_PATH, getBackdropPath());
        contentValues.put(PrivaliaContract.MovieEntry.POPULARITY, getPopularity());
        contentValues.put(PrivaliaContract.MovieEntry.VOTE_COUNT, getVoteCount());
        contentValues.put(PrivaliaContract.MovieEntry.VIDEO, Boolean.toString(isVideo()));
        contentValues.put(PrivaliaContract.MovieEntry.VOTE_AVERAGE, getVoteAverage());
        contentValues.put(PrivaliaContract.MovieEntry.REGISTRY_TYPE, getRegistryType());

        return contentValues;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.LOG_TAG);
        dest.writeInt(this.id);
        dest.writeString(this.posterPath);
        dest.writeByte(this.adult ? (byte) 1 : (byte) 0);
        dest.writeString(this.overview);
        dest.writeString(this.releaseDate);
        dest.writeString(this.movieWebId);
        dest.writeString(this.originalTitle);
        dest.writeString(this.originalLanguage);
        dest.writeString(this.title);
        dest.writeString(this.backdropPath);
        dest.writeDouble(this.popularity);
        dest.writeInt(this.voteCount);
        dest.writeByte(this.video ? (byte) 1 : (byte) 0);
        dest.writeDouble(this.voteAverage);
        dest.writeString(this.registryType);
        dest.writeString(this.timestamp);
    }

    protected Movie(Parcel in) {
        this.LOG_TAG = in.readString();
        this.id = in.readInt();
        this.posterPath = in.readString();
        this.adult = in.readByte() != 0;
        this.overview = in.readString();
        this.releaseDate = in.readString();
        this.movieWebId = in.readString();
        this.originalTitle = in.readString();
        this.originalLanguage = in.readString();
        this.title = in.readString();
        this.backdropPath = in.readString();
        this.popularity = in.readDouble();
        this.voteCount = in.readInt();
        this.video = in.readByte() != 0;
        this.voteAverage = in.readDouble();
        this.registryType = in.readString();
        this.timestamp = in.readString();
    }

    public static final Parcelable.Creator<Movie> CREATOR = new Parcelable.Creator<Movie>() {
        @Override
        public Movie createFromParcel(Parcel source) {
            return new Movie(source);
        }

        @Override
        public Movie[] newArray(int size) {
            return new Movie[size];
        }
    };
}
