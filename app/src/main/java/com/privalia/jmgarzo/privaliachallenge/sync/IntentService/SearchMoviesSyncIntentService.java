package com.privalia.jmgarzo.privaliachallenge.sync.IntentService;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;

import com.privalia.jmgarzo.privaliachallenge.sync.SyncTasks;

/**
 * Created by jmgar on 26/01/2018.
 */

public class SearchMoviesSyncIntentService extends IntentService {
    public static final String QUERY_ARG_TAG = "query_arg_tag";


    public SearchMoviesSyncIntentService(){
        super("SearchMoviesSyncIntentService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        if(null!=intent.getStringExtra(QUERY_ARG_TAG)) {
            String query = intent.getStringExtra(QUERY_ARG_TAG);
            SyncTasks.syncSearchMovies(this, query);
        }
    }
}
