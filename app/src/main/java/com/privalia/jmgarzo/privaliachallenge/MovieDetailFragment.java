package com.privalia.jmgarzo.privaliachallenge;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.privalia.jmgarzo.privaliachallenge.model.Movie;
import com.privalia.jmgarzo.privaliachallenge.utilities.NetworkUtils;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A placeholder fragment containing a simple view.
 */
public class MovieDetailFragment extends Fragment {

    private Movie mMovie;

    @BindView(R.id.poster_iv)
    ImageView mPosterImage;
    @BindView(R.id.movie_release_date)
    TextView mReleaseDate;
    @BindView(R.id.vote_average)
    TextView mVoteAverage;
    @BindView(R.id.overview_text)
    TextView mOverviewText;


    public MovieDetailFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view =  inflater.inflate(R.layout.fragment_movie_detail, container, false);

        ButterKnife.bind(this,view);

        Intent intent = getActivity().getIntent();
        if (null != intent && intent.hasExtra(MainActivity.MOVIE_INTENT_TAG)) {
            mMovie = intent.getParcelableExtra(MainActivity.MOVIE_INTENT_TAG);
        }
        if(null != mMovie){
            getActivity().setTitle(mMovie.getTitle());
            Picasso.with(getActivity())
                    .load(NetworkUtils.buildPosterDetail(mMovie.getPosterPath()))
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.ic_broken_image_black_48px)
                    .tag(getActivity())
                    .into(mPosterImage);

            mOverviewText.setText(mMovie.getOverview());
            mReleaseDate.setText(mMovie.getReleaseDate());
            mVoteAverage.setText(Double.toString(mMovie.getVoteAverage()).concat("/10"));



        }

        return view;
    }
}
