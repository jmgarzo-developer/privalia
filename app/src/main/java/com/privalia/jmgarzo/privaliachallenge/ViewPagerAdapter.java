package com.privalia.jmgarzo.privaliachallenge;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by jmgar on 24/01/2018.
 */

public class ViewPagerAdapter extends FragmentStatePagerAdapter {

    int tabCount;

    public ViewPagerAdapter(FragmentManager fm, int tabCount) {
        super(fm);
        this.tabCount= tabCount;
    }

    @Override
    public Fragment getItem(int position) {
        //Returning the current tabs
        switch (position) {
            case 0:
                return new MostPopularFragment();
            case 1:
                return new SearchFragment();
//            case 2:
//                return new TvMostPopularFragment();
//            case 3:
//                return new TvTopRateFragment();
//            case 4:
//                return new MovieFavoriteFragment();

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return tabCount;
    }

}
