package com.privalia.jmgarzo.privaliachallenge;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.privalia.jmgarzo.privaliachallenge.model.Movie;
import com.privalia.jmgarzo.privaliachallenge.sync.SyncUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements
        TabLayout.OnTabSelectedListener, MostPopularFragment.Callback,
        SearchFragment.Callback {
    private static int mPosition = 0;
    private static final String POSITION_TAG = "position_tag";
    public static final String MOVIE_INTENT_TAG = "movie_intent_tag";


    @BindView(R.id.tabLayout)
    TabLayout mTabLayout;

    @BindView(R.id.pager)
    ViewPager mViewPager;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        setSupportActionBar(mToolbar);
        mToolbar.setLogo(R.mipmap.ic_launcher);


        if (savedInstanceState != null) {
            mPosition = savedInstanceState.getInt(POSITION_TAG);
        }
        SyncUtils.initialize(this);


        // mTabLayout = (TabLayout) findViewById(R.id.tabLayout);
        mTabLayout.setTabMode(TabLayout.GRAVITY_CENTER);
        mTabLayout.addTab(mTabLayout.newTab().setText(getString(R.string.title_tab_movies_most_popular)));
        mTabLayout.addTab(mTabLayout.newTab().setText(getString(R.string.title_tab_search)));

        mTabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        mViewPager.setCurrentItem(2);


        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager(), mTabLayout.getTabCount());

        //Adding adapter to pager
        mViewPager.setAdapter(adapter);
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabLayout));

        //Adding onTabSelectedListener to swipe views
        mTabLayout.addOnTabSelectedListener(this);

        mViewPager.setCurrentItem(mPosition);


    }


    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        mViewPager.setCurrentItem(tab.getPosition());

    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    //We are using the same for MostPopularFragment.Callback and SearchFragment.Callback
    @Override
    public void OnMovieItemSelected(Movie movie) {
        Intent intent = new Intent(this, MovieDetailActivity.class);
        intent.putExtra(MOVIE_INTENT_TAG, movie);
        startActivity(intent);
    }
}
