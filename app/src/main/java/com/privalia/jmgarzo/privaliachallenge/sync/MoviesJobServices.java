package com.privalia.jmgarzo.privaliachallenge.sync;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.RequiresApi;

import com.firebase.jobdispatcher.JobService;

/**
 * Created by jmgar on 24/01/2018.
 */

@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)

public class MoviesJobServices extends JobService {

    private static AsyncTask<Void, Void, Void> mFetchMoviesTask;


    @Override
    public boolean onStartJob(final com.firebase.jobdispatcher.JobParameters params) {
         mFetchMoviesTask = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                Context context = getApplicationContext();
                SyncTasks.syncMostPopularMovies(context);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                jobFinished(params, false);
            }
        };
        mFetchMoviesTask.execute();
        return true;
    }

    @Override
    public boolean onStopJob(com.firebase.jobdispatcher.JobParameters job) {
        return false;
    }
}
