package com.privalia.jmgarzo.privaliachallenge.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by jmgar on 24/01/2018.
 */

public class PrivaliaDBHelper extends SQLiteOpenHelper {

    private String LOG_TAG = PrivaliaDBHelper.class.getSimpleName();


    public static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "movie.db";

    Context mContext;


    private static final String SQL_CREATE_MOVIE_TABLE =
            "CREATE TABLE " + PrivaliaContract.MovieEntry.TABLE_NAME + " ( " +
                    PrivaliaContract.MovieEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT , " +
                    PrivaliaContract.MovieEntry.POSTER_PATH + " TEXT NOT NULL, " +
                    PrivaliaContract.MovieEntry.ADULT + " TEXT NOT NULL, " +
                    PrivaliaContract.MovieEntry.OVERVIEW + " TEXT NOT NULL, " +
                    PrivaliaContract.MovieEntry.RELEASE_DATE + " TEXT NOT NULL, " +
                    PrivaliaContract.MovieEntry.MOVIE_WEB_ID + " TEXT NOT NULL, " +
                    PrivaliaContract.MovieEntry.ORIGINAL_TITLE + " TEXT NOT NULL, " +
                    PrivaliaContract.MovieEntry.ORIGINAL_LANGUAGE + " TEXT NOT NULL, " +
                    PrivaliaContract.MovieEntry.TITLE + " TEXT NOT NULL, " +
                    PrivaliaContract.MovieEntry.BACKDROP_PATH + " TEXT NOT NULL, " +
                    PrivaliaContract.MovieEntry.POPULARITY + " REAL NOT NULL, " +
                    PrivaliaContract.MovieEntry.VOTE_COUNT + " INTEGER NOT NULL, " +
                    PrivaliaContract.MovieEntry.VIDEO + " TEXT NOT NULL, " +
                    PrivaliaContract.MovieEntry.VOTE_AVERAGE + " REAL NOT NULL, " +
                    PrivaliaContract.MovieEntry.REGISTRY_TYPE + " TEXT NOT NULL , " +
                    PrivaliaContract.MovieEntry.TIMESTAMP + " TIMESTAMP DEFAULT CURRENT_TIMESTAMP " +
                    " );";


    public PrivaliaDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        mContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(SQL_CREATE_MOVIE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + PrivaliaContract.MovieEntry.TABLE_NAME);

    }
}
