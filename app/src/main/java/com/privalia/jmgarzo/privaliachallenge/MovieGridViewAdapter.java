package com.privalia.jmgarzo.privaliachallenge;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.privalia.jmgarzo.privaliachallenge.data.PrivaliaContract;
import com.privalia.jmgarzo.privaliachallenge.model.Movie;
import com.privalia.jmgarzo.privaliachallenge.sync.IntentService.AddMoviePageIntentService;
import com.privalia.jmgarzo.privaliachallenge.utilities.DatabaseUtil;
import com.privalia.jmgarzo.privaliachallenge.utilities.NetworkUtils;
import com.squareup.picasso.Picasso;

/**
 * Created by jmgar on 24/01/2018.
 */

public class MovieGridViewAdapter extends RecyclerView.Adapter<MovieGridViewAdapter.MovieAdapterViewHolder> {




    private Context mContext;
    private String mRegistryType;

    private final MovieGridViewAdapterOnClickHandler mClickHandler;


    public interface MovieGridViewAdapterOnClickHandler {

        void onClick(Movie movie);
    }

    private Cursor mCursor;
    private static int mPageMostPopular = 1;

    public MovieGridViewAdapter(@NonNull Context context, MovieGridViewAdapterOnClickHandler clickHandler, String registryType) {
        mContext = context;
        //TODO: made (or delete this code about clickhandler) a new detail activity
        mClickHandler = clickHandler;
        mRegistryType = registryType;
    }

    public static void setmPageMostPopular(int mPageMostPopular) {
        MovieGridViewAdapter.mPageMostPopular = mPageMostPopular;
    }


    @Override
    public MovieAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.movie_gridview_item, parent, false);
        view.setFocusable(true);
        return new MovieAdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MovieAdapterViewHolder holder, int position) {

        mCursor.moveToPosition(position);
        String posterPath = mCursor.getString(DatabaseUtil.COL_MOVIE_POSTER_PATH);

        String posterUrl = NetworkUtils.buildPosterThumbnail(posterPath);
        Picasso.with(mContext)
                .load(posterUrl)
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.ic_broken_image_black_48px)
                .tag(mContext)
                .into(holder.mMovieThumb);

        if (position == getItemCount() - 3) {
            loadNextPage();
        }
    }

    private void loadNextPage(){
        if(mRegistryType.equals(PrivaliaContract.MOST_POPULAR_REGISTRY_TYPE)){
            mPageMostPopular++;// = (getItemCount() / 20) + 1;
            Intent addNewPage = new Intent(mContext, AddMoviePageIntentService.class);
            addNewPage.putExtra(AddMoviePageIntentService.REGISTRY_TYPE_TAG, PrivaliaContract.MOST_POPULAR_REGISTRY_TYPE);
            addNewPage.putExtra(AddMoviePageIntentService.PAGE_NUMBER_TAG, Integer.toString(mPageMostPopular));
            mContext.startService(addNewPage);
        }
    }

    @Override
    public int getItemCount() {
        if (null == mCursor) return 0;
        return mCursor.getCount();
    }

//    public void setMovies(ArrayList<Movie>moviesList){
//        mMoviesList = moviesList;
//        notifyDataSetChanged();
//    }

    void swapCursor(Cursor newCursor) {
        mCursor = newCursor;
        notifyDataSetChanged();
    }



    public class MovieAdapterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public final ImageView mMovieThumb;

        public MovieAdapterViewHolder(View view) {
            super(view);
            mMovieThumb = (ImageView) view.findViewById(R.id.iv_movie_thumb);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int adapterPosition = getAdapterPosition();
            Movie movie = new Movie(mCursor, adapterPosition);
            mClickHandler.onClick(movie);
        }
    }
}
