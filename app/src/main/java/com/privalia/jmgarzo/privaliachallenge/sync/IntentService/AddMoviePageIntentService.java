package com.privalia.jmgarzo.privaliachallenge.sync.IntentService;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;

import com.privalia.jmgarzo.privaliachallenge.data.PrivaliaContract;
import com.privalia.jmgarzo.privaliachallenge.sync.SyncTasks;

/**
 * Created by jmgar on 25/01/2018.
 */

public class AddMoviePageIntentService extends IntentService {

    public final static String REGISTRY_TYPE_TAG = "registry_type_tag";
    public final static String PAGE_NUMBER_TAG = "page_number_tag";
    public final static String QUERY_ARG_TAG = "query_arg_tag";


    public AddMoviePageIntentService() {
        super("AddMoviePageIntentService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        if (intent != null && null != intent.getStringExtra(REGISTRY_TYPE_TAG) && null != intent.getStringExtra(PAGE_NUMBER_TAG)) {
            String registryType = intent.getStringExtra(REGISTRY_TYPE_TAG);
            String pageNumber = intent.getStringExtra(PAGE_NUMBER_TAG);
            if (registryType.equals(PrivaliaContract.MOST_POPULAR_REGISTRY_TYPE)) {
                SyncTasks.addPageMovies(this, registryType, pageNumber);
            }else if (registryType.equals(PrivaliaContract.SEARCH_REGISTRY_TYPE)){
                if(null!= intent.getStringExtra(QUERY_ARG_TAG)){
                    String queryArg = intent.getStringExtra(QUERY_ARG_TAG);
                    SyncTasks.addPageMovies(this,registryType,pageNumber,queryArg);
                }
            }
        }
    }
}
