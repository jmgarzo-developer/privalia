package com.privalia.jmgarzo.privaliachallenge.utilities;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import com.privalia.jmgarzo.privaliachallenge.BuildConfig;
import com.privalia.jmgarzo.privaliachallenge.data.PrivaliaContract;
import com.privalia.jmgarzo.privaliachallenge.model.Movie;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by jmgar on 24/01/2018.
 */

public class NetworkUtils {

    private final static String LOG_TAG = NetworkUtils.class.getSimpleName();

    private static final String MOVIES_BASE_URL = "https://api.themoviedb.org/3/";
    private static final String IMAGE_BASE_URL = "http://image.tmdb.org/t/p/";



    public static final String MOVIE_MOST_POPULAR = "movie/popular";
    public static final String MOVIE_SEARCH = "search/movie";

    private static final String API_KEY_PARAM = "api_key";
    private static final String API_MOVIE_PARAM = "movie";
    private static final String API_PAGE_PARAM = "page";
    private static final String API_SEARCH_PARAM = "query";
    private static final String API_INCLUDE_ADULT_PARAM="include_adult";

    private static final String SIZE_w92 = "w92";
    private static final String SIZE_w154 = "w154";
    private static final String SIZE_w185 = "w185";
    private static final String SIZE_w342 = "w342";
    private static final String SIZE_w500 = "w500";
    private static final String SIZE_w780 = "w780";


    //MOVIE JSON CONSTANTES
    private static final String POSTER_PATH = "poster_path";
    private static final String ADULT = "adult";
    private static final String OVERVIEW = "overview";
    private static final String RELEASE_DATE = "release_date";
    private static final String WEB_MOVIE_ID = "id";
    private static final String ORIGINAL_TITLE = "original_title";
    private static final String ORIGINAL_LANGUAGE = "original_language";
    private static final String TITLE = "title";
    private static final String BACKDROP_PATH = "backdrop_path";
    private static final String POPULARITY = "popularity";
    private static final String VOTE_COUNT = "vote_count";
    private static final String VIDEO = "video" ;
    private static final String VOTE_AVERAGE = "vote_average" ;




    /**
     * This method returns most popular movies from API
     *
     * @param context
     * @return ArrayList<Movie>
     */
    public static ArrayList<Movie> getMovies(Context context, String registryType, String page,String searchArg){
        URL moviesURL;
        if(registryType.equals(PrivaliaContract.MOST_POPULAR_REGISTRY_TYPE)) {
            moviesURL = NetworkUtils.buildPageMovieURL(context, MOVIE_MOST_POPULAR, page,null);
        }else  {
            moviesURL = NetworkUtils.buildPageMovieURL(context, MOVIE_SEARCH,page,searchArg);
        }

        ArrayList<Movie> moviesList = null;
        try {
            String jsonMoviesResponse = NetworkUtils.getResponseFromHttpUrl(moviesURL);
            moviesList = getMoviesFromJson(jsonMoviesResponse,registryType);

        } catch (IOException e) {
            Log.e(LOG_TAG, e.toString());
        }

        return moviesList;
    }
    public static ArrayList<Movie> getMostPopularMovies(Context context, String registryType, String page) {
        return getMovies(context,registryType,page,null);
    }
    public static ArrayList<Movie> getSearchMovies(Context context,String registryType, String page, String searchArg){
        return getMovies(context,registryType,page,searchArg);
    }




    public static URL buildPageMovieURL(Context context, String registryType, String page,String searchArg) {
        Uri builtUri = null;
        if(registryType.equalsIgnoreCase(MOVIE_MOST_POPULAR)) {
            builtUri = Uri.parse(MOVIES_BASE_URL).buildUpon()
                    .appendEncodedPath(registryType)
                    .appendQueryParameter(API_KEY_PARAM, BuildConfig.THE_MOVIE_DB_API_KEY)
                    .appendQueryParameter(API_PAGE_PARAM, page)
                    .build();
        }else if(registryType.equalsIgnoreCase(MOVIE_SEARCH))
            if(searchArg != null) {
            builtUri = Uri.parse(MOVIES_BASE_URL).buildUpon()
                    .appendEncodedPath(registryType)
                    .appendQueryParameter(API_KEY_PARAM, BuildConfig.THE_MOVIE_DB_API_KEY)
                    .appendQueryParameter(API_PAGE_PARAM, page)
                    .appendQueryParameter(API_SEARCH_PARAM, searchArg)
                    .appendQueryParameter(API_INCLUDE_ADULT_PARAM,"false")
                    .build();
        }

        URL url = null;
        try {
            if(builtUri != null)
            url = new URL(builtUri.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        Log.v(LOG_TAG, "Built URI " + url);
        return url;
    }



    /**
     * This method returns the entire result from the HTTP response.
     *
     * @param url The URL to fetch the HTTP response from.
     * @return The contents of the HTTP response.
     * @throws IOException Related to network and stream reading
     */
    public static String getResponseFromHttpUrl(URL url) throws IOException {
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        try {
            InputStream in = urlConnection.getInputStream();

            Scanner scanner = new Scanner(in);
            scanner.useDelimiter("\\A");

            boolean hasInput = scanner.hasNext();
            if (hasInput) {
                return scanner.next();
            } else {
                return null;
            }
        } finally {
            urlConnection.disconnect();
        }
    }


    private static ArrayList<Movie> getMoviesFromJson(String moviesJsonStr,String registryType) {
        final String MOVIE_RESULTS = "results";


        ArrayList<Movie> moviesList = null;

        JSONObject moviesJson = null;
        try {
            moviesJson = new JSONObject(moviesJsonStr);
            JSONArray moviesArray = moviesJson.getJSONArray(MOVIE_RESULTS);
            moviesList = new ArrayList<>();
            for (int i = 0; i < moviesArray.length(); i++) {
                JSONObject jsonMovie = moviesArray.getJSONObject(i);
                Movie movie = new Movie();
                movie.setPosterPath(jsonMovie.getString(POSTER_PATH));
                movie.setAdult(Boolean.valueOf(jsonMovie.getString(ADULT)));
                movie.setOverview(jsonMovie.getString(OVERVIEW));
                movie.setReleaseDate(jsonMovie.getString(RELEASE_DATE));
                movie.setMovieWebId(jsonMovie.getString(WEB_MOVIE_ID));
                movie.setOriginalTitle(jsonMovie.getString(ORIGINAL_TITLE));
                movie.setOriginalLanguage(jsonMovie.getString(ORIGINAL_LANGUAGE));
                movie.setTitle(jsonMovie.getString(TITLE));
                movie.setBackdropPath(jsonMovie.getString(BACKDROP_PATH));
                movie.setPopularity(jsonMovie.getDouble(POPULARITY));
                movie.setVoteCount(jsonMovie.getInt(VOTE_COUNT));
                movie.setVideo(Boolean.valueOf(jsonMovie.getString(VIDEO)));
                movie.setVoteAverage(jsonMovie.getDouble(VOTE_AVERAGE));
                movie.setRegistryType(registryType);
                moviesList.add(movie);
            }

        } catch (JSONException e) {
            Log.e(LOG_TAG, e.toString());
        }

        return moviesList;
    }



    /**
     * This method returns poster URL
     *
     * @param posterPath
     * @param size
     * @return
     */
    public static URL buildPosterUrl(String posterPath, String size) {

        Uri builtUri = Uri.parse(IMAGE_BASE_URL).buildUpon()
                .appendEncodedPath(size)
                .appendEncodedPath(posterPath)
                .build();

        URL url = null;
        try {
            url = new URL(builtUri.toString());
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, e.toString());
        }

        return url;
    }

    public static String buildPosterThumbnail(String posterPath) {
        return buildPosterUrl(posterPath, SIZE_w185).toString();
    }

    public static String buildPosterDetail(String posterPath) {
        return buildPosterUrl(posterPath, SIZE_w342).toString();
    }

}
