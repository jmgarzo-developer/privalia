package com.privalia.jmgarzo.privaliachallenge.sync;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.util.Log;

import com.privalia.jmgarzo.privaliachallenge.data.PrivaliaContract;
import com.privalia.jmgarzo.privaliachallenge.model.Movie;
import com.privalia.jmgarzo.privaliachallenge.utilities.NetworkUtils;

import java.util.ArrayList;

/**
 * Created by jmgar on 24/01/2018.
 */

public class SyncTasks {

    private static final String LOG_TAG = SyncTasks.class.getSimpleName();


    /**
     * This method make a new ask to API to get new data movies, and delete the old records
     * except favorite.
     *
     * @param context
     */
    synchronized public static void syncMostPopularMovies(Context context) {
        try {
            ArrayList<Movie> moviesList = NetworkUtils.getMostPopularMovies(context, PrivaliaContract.MOST_POPULAR_REGISTRY_TYPE, "1");

            if (moviesList != null && moviesList.size() > 0) {
                ContentValues[] contentValues = new ContentValues[moviesList.size()];
                for (int i = 0; i < moviesList.size(); i++) {
                    Movie movie = moviesList.get(i);
                    contentValues[i] = movie.getContentValues();
                }

                ContentResolver contentResolver = context.getContentResolver();
                contentResolver.delete(
                        PrivaliaContract.MovieEntry.CONTENT_URI,
                        PrivaliaContract.MovieEntry.REGISTRY_TYPE + " == ? ",
                        new String[]{PrivaliaContract.MOST_POPULAR_REGISTRY_TYPE});

                contentResolver.bulkInsert(PrivaliaContract.MovieEntry.CONTENT_URI,
                        contentValues);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    synchronized public static void syncSearchMovies(Context context, String searchArg) {
        try {
            ContentResolver contentResolver = context.getContentResolver();

            if (searchArg.length() == 0) {
                contentResolver.delete(
                        PrivaliaContract.MovieEntry.CONTENT_URI,
                        PrivaliaContract.MovieEntry.REGISTRY_TYPE + " = ? ",
                        new String[]{PrivaliaContract.SEARCH_REGISTRY_TYPE});
            }
            ArrayList<Movie> moviesList = NetworkUtils.getSearchMovies(context, PrivaliaContract.SEARCH_REGISTRY_TYPE, "1", searchArg);

            if (moviesList != null) {
                if(!moviesList.isEmpty()) {
                    ContentValues[] contentValues = new ContentValues[moviesList.size()];
                    for (int i = 0; i < moviesList.size(); i++) {
                        Movie movie = moviesList.get(i);
                        contentValues[i] = movie.getContentValues();
                    }

                    contentResolver.delete(
                            PrivaliaContract.MovieEntry.CONTENT_URI,
                            PrivaliaContract.MovieEntry.REGISTRY_TYPE + " = ? ",
                            new String[]{PrivaliaContract.SEARCH_REGISTRY_TYPE});

                    contentResolver.bulkInsert(PrivaliaContract.MovieEntry.CONTENT_URI,
                            contentValues);
                }else{
                    contentResolver.delete(
                            PrivaliaContract.MovieEntry.CONTENT_URI,
                            PrivaliaContract.MovieEntry.REGISTRY_TYPE + " = ? ",
                            new String[]{PrivaliaContract.SEARCH_REGISTRY_TYPE});
                }
            }
        } catch (Exception e) {
            Log.e(LOG_TAG, e.toString());
        }
    }


    synchronized public static void addPageMovies(Context context, String registryType, String page, String queryArg) {
        try {
            ArrayList<Movie> moviesList = null;
            if (registryType.equals(PrivaliaContract.MOST_POPULAR_REGISTRY_TYPE)) {
                moviesList = NetworkUtils.getMostPopularMovies(context, registryType, page);
            } else if (registryType.equals(PrivaliaContract.SEARCH_REGISTRY_TYPE)) {
                moviesList = NetworkUtils.getSearchMovies(context, registryType, page, queryArg);
            }


            if (moviesList != null && moviesList.size() > 0) {
                ContentValues[] contentValues = new ContentValues[moviesList.size()];
                for (int i = 0; i < moviesList.size(); i++) {
                    Movie movie = moviesList.get(i);
                    contentValues[i] = movie.getContentValues();
                }

                ContentResolver contentResolver = context.getContentResolver();

                contentResolver.bulkInsert(PrivaliaContract.MovieEntry.CONTENT_URI,
                        contentValues);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    synchronized public static void addPageMovies(Context context, String registryType, String page) {
        addPageMovies(context, registryType, page, null);
    }


    synchronized public static void refreshMovieData(Context context, String registryType) {
        String selection = PrivaliaContract.MovieEntry.REGISTRY_TYPE + " = ? AND " +
                PrivaliaContract.MovieEntry.REGISTRY_TYPE + " <> ? ";

        context.getContentResolver().delete(PrivaliaContract.MovieEntry.CONTENT_URI,
                selection,
                new String[]{registryType, PrivaliaContract.SEARCH_REGISTRY_TYPE});
        addPageMovies(context, registryType, "1");

    }
}
