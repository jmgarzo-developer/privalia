package com.privalia.jmgarzo.privaliachallenge;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.privalia.jmgarzo.privaliachallenge.data.PrivaliaContract;
import com.privalia.jmgarzo.privaliachallenge.model.Movie;
import com.privalia.jmgarzo.privaliachallenge.sync.IntentService.AddMoviePageIntentService;
import com.privalia.jmgarzo.privaliachallenge.utilities.DatabaseUtil;
import com.privalia.jmgarzo.privaliachallenge.utilities.NetworkUtils;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by jmgar on 26/01/2018.
 */

public class MovieSearchAdapter extends RecyclerView.Adapter<MovieSearchAdapter.SearchMovieAdapterViewHolder>{



    private Context mContext;
    private String mRegistryType;

    private final MovieSearchAdapterOnClickHandler mClickHandler;


    public interface MovieSearchAdapterOnClickHandler {

        void onClick(Movie movie);
    }

    private Cursor mCursor;
    private static int mPageSearch = 1;
    private String mQueryArg;



    public MovieSearchAdapter(@NonNull Context context, MovieSearchAdapterOnClickHandler clickHandler, String registryType, String queryArg) {
        mContext = context;
        //TODO: made (or delete this code about clickhandler) a new detail activity
        mClickHandler = clickHandler;
        mRegistryType = registryType;
        mQueryArg = queryArg;


    }

    public static void setPageSeachPopular(int page) {
        MovieSearchAdapter.mPageSearch = page;
    }

    private void loadNextPage(){
        if(mRegistryType.equals(PrivaliaContract.SEARCH_REGISTRY_TYPE)){
            mPageSearch++;// = (getItemCount() / 20) + 1;
            Intent addNewPage = new Intent(mContext, AddMoviePageIntentService.class);
            addNewPage.putExtra(AddMoviePageIntentService.REGISTRY_TYPE_TAG, PrivaliaContract.SEARCH_REGISTRY_TYPE);
            addNewPage.putExtra(AddMoviePageIntentService.PAGE_NUMBER_TAG, Integer.toString(mPageSearch));
            addNewPage.putExtra(AddMoviePageIntentService.QUERY_ARG_TAG,mQueryArg);
            mContext.startService(addNewPage);
        }
    }


    @Override
    public SearchMovieAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.search_movie_item, parent, false);
        view.setFocusable(true);
        return new SearchMovieAdapterViewHolder(view);    }

    @Override
    public void onBindViewHolder(SearchMovieAdapterViewHolder holder, int position) {


        mCursor.moveToPosition(position);
        String posterPath = mCursor.getString(DatabaseUtil.COL_MOVIE_POSTER_PATH);

        String posterUrl = NetworkUtils.buildPosterThumbnail(posterPath);
        Picasso.with(mContext)
                .load(posterUrl)
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.ic_broken_image_black_48px)
                .tag(mContext)
                .into(holder.mPosterImage);

        String sDate = mCursor.getString(DatabaseUtil.COL_MOVIE_RELEASE_DATE);
        String [] splitDate = sDate.split("-");
        String title = (mCursor.getString(DatabaseUtil.COL_MOVIE_TITLE)+ " (" + splitDate[0] + ")");
        holder.mTitle.setText(title);
        holder.mOverview.setText(mCursor.getString(DatabaseUtil.COL_MOVIE_OVERVIEW));

        if (position == getItemCount() - 3) {
            loadNextPage();
        }
    }

    @Override
    public int getItemCount() {
        if (null == mCursor) return 0;
        return mCursor.getCount();
    }

    void swapCursor(Cursor newCursor) {
        mCursor = newCursor;
        notifyDataSetChanged();
    }
    void setQueryArg(String queryArg){
        mQueryArg= queryArg;
    }

    public void setmPageSearch(int pageSearch) {
        mPageSearch = pageSearch;
        notifyDataSetChanged();
    }

    public class SearchMovieAdapterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.item_search_poster)
        ImageView mPosterImage;
        @BindView(R.id.item_search_title)
        TextView mTitle;
        @BindView(R.id.item_search_overview)
        TextView mOverview;

        public SearchMovieAdapterViewHolder(View view) {
            super(view);
            ButterKnife.bind(this,view);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int adapterPosition = getAdapterPosition();
            Movie movie = new Movie(mCursor, adapterPosition);
            mClickHandler.onClick(movie);
        }
    }
}
