package com.privalia.jmgarzo.privaliachallenge.utilities;

import com.privalia.jmgarzo.privaliachallenge.data.PrivaliaContract;

/**
 * Created by jmgar on 24/01/2018.
 */

public class DatabaseUtil {

    public static final String[] MOVIE_COLUMNS = {
            PrivaliaContract.MovieEntry._ID,
            PrivaliaContract.MovieEntry.POSTER_PATH,
            PrivaliaContract.MovieEntry.ADULT,
            PrivaliaContract.MovieEntry.OVERVIEW,
            PrivaliaContract.MovieEntry.RELEASE_DATE,
            PrivaliaContract.MovieEntry.MOVIE_WEB_ID,
            PrivaliaContract.MovieEntry.ORIGINAL_TITLE,
            PrivaliaContract.MovieEntry.ORIGINAL_LANGUAGE,
            PrivaliaContract.MovieEntry.TITLE,
            PrivaliaContract.MovieEntry.BACKDROP_PATH,
            PrivaliaContract.MovieEntry.POPULARITY,
            PrivaliaContract.MovieEntry.VOTE_COUNT,
            PrivaliaContract.MovieEntry.VIDEO,
            PrivaliaContract.MovieEntry.VOTE_AVERAGE,
            PrivaliaContract.MovieEntry.REGISTRY_TYPE,
            PrivaliaContract.MovieEntry.TIMESTAMP
    };

    public static final int COL_MOVIE_ID = 0;
    public static final int COL_MOVIE_POSTER_PATH = 1;
    public static final int COL_MOVIE_ADULT = 2;
    public static final int COL_MOVIE_OVERVIEW = 3;
    public static final int COL_MOVIE_RELEASE_DATE = 4;
    public static final int COL_MOVIE_MOVIE_WEB_ID = 5;
    public static final int COL_MOVIE_ORIGINAL_TITLE = 6;
    public static final int COL_MOVIE_ORIGINAL_LANGUAGE = 7;
    public static final int COL_MOVIE_TITLE = 8;
    public static final int COL_MOVIE_BACKDROP_PATH = 9;
    public static final int COL_MOVIE_POPULARITY = 10;
    public static final int COL_MOVIE_VOTE_COUNT = 11;
    public static final int COL_MOVIE_VIDEO = 12;
    public static final int COL_MOVIE_VOTE_AVERAGE = 13;
    public static final int COL_MOVIE_REGISTRY_TYPE = 14;
    public static final int COL_MOVIE_TIMESTAMP = 15;
}
