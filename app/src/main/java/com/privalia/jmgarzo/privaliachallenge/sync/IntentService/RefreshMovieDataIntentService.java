package com.privalia.jmgarzo.privaliachallenge.sync.IntentService;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;

import com.privalia.jmgarzo.privaliachallenge.sync.SyncTasks;

/**
 * Created by jmgar on 25/01/2018.
 */

public class RefreshMovieDataIntentService extends IntentService {
    public static final String MOVIE_REGISTRY_TYPE_TAG = "movie_registry_type_tag";

    public RefreshMovieDataIntentService(){
        super("RefreshMovieDataIntentService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        if(null!=intent.getStringExtra(MOVIE_REGISTRY_TYPE_TAG)){
            String registryType = intent.getStringExtra(MOVIE_REGISTRY_TYPE_TAG);
            SyncTasks.refreshMovieData(this,registryType);
        }
    }
}