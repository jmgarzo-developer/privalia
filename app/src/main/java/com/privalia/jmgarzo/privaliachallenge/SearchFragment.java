package com.privalia.jmgarzo.privaliachallenge;


import android.app.SearchManager;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.privalia.jmgarzo.privaliachallenge.data.PrivaliaContract;
import com.privalia.jmgarzo.privaliachallenge.model.Movie;
import com.privalia.jmgarzo.privaliachallenge.sync.SearchMoviesAsyncTask;
import com.privalia.jmgarzo.privaliachallenge.utilities.DatabaseUtil;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * A simple {@link Fragment} subclass.
 */
public class SearchFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>,
        MovieSearchAdapter.MovieSearchAdapterOnClickHandler {

    private static final int ID_MOVIE_SEARCH_LOADER = 45;
    private static final String SEARCH_TEXT_TAG = "search_text_tag";


    @BindView(R.id.recyclerview_search_movie)
    RecyclerView mRecyclerView;

    @BindView(R.id.progress_bar_search_fragment)
    ProgressBar mProgressBar;

    SearchView mSearchView = null;


    Bundle mSavedInstanceState;
    MovieSearchAdapter mMovieSearchAdapter;

    private String mSearchViewText = "";
    private AsyncTask<String, Void, Void> mSearchMoviesAsyncTask;

    public interface Callback {
        public void OnMovieItemSelected(Movie movie);
    }
    public SearchFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View viewRoot = inflater.inflate(R.layout.fragment_search, container, false);


        mSavedInstanceState = savedInstanceState;
//        if (mSavedInstanceState != null) {
//            mSearchViewText = savedInstanceState.getString(SEARCH_TEXT_TAG);
//            if (mSearchView != null) {
//                mSearchView.setQuery(mSearchViewText, false);
//            }
//        }
        ButterKnife.bind(this, viewRoot);


        setHasOptionsMenu(true);


        LinearLayoutManager movieSearchLayoutManager =
                new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);

        mRecyclerView.setLayoutManager(movieSearchLayoutManager);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(),
                DividerItemDecoration.VERTICAL));

        mMovieSearchAdapter = new MovieSearchAdapter(getContext(), this, PrivaliaContract.SEARCH_REGISTRY_TYPE, mSearchViewText);
        mRecyclerView.setAdapter(mMovieSearchAdapter);
        getActivity().getSupportLoaderManager().initLoader(ID_MOVIE_SEARCH_LOADER, null, this);

        return viewRoot;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if (!mSearchViewText.isEmpty()) {
            outState.putString(SEARCH_TEXT_TAG, mSearchViewText);
        }
        super.onSaveInstanceState(outState);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.menu_search_fragment, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        if (searchItem != null) {
            mSearchView = (SearchView) searchItem.getActionView();
        }
        if (mSearchView != null) {

            mSearchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
            mSearchView.setActivated(true);
            mSearchView.setQueryHint(getString(R.string.search_hint));
            mSearchView.setIconifiedByDefault(false);
            mSearchView.requestFocus();
            mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String newText) {

                    onQueryTextChanged(newText);
                    return false;
                }
            });

            if (null != mSearchViewText && !mSearchViewText.isEmpty()) {

                mSearchView.setIconified(false);
                mSearchView.setQuery(mSearchViewText, false);
                //mSearchView.onActionViewExpanded();

            }
            if (mSavedInstanceState != null) {

                mSearchViewText = mSavedInstanceState.getString(SEARCH_TEXT_TAG);
                mSearchView.setIconified(false);
                mSearchView.setQuery(mSearchViewText, false);
//                mSearchView.onActionViewExpanded();

            }
        }
    }

    public boolean onQueryTextChanged(String newText) {
        if(newText.equals(mSearchViewText)){
            return true;
        }

        mProgressBar.setVisibility(View.VISIBLE);
        mSearchViewText = newText;
        mMovieSearchAdapter.setQueryArg(newText);

        if (null != mSearchMoviesAsyncTask) {
            mSearchMoviesAsyncTask.cancel(true);
        }
        mMovieSearchAdapter.setmPageSearch(1);
        mSearchMoviesAsyncTask = new SearchMoviesAsyncTask(getContext()).execute(newText);

        return true;
    }


    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        switch (id) {
            case ID_MOVIE_SEARCH_LOADER: {
                return new CursorLoader(
                        getContext(),
                        PrivaliaContract.MovieEntry.CONTENT_URI,
                        DatabaseUtil.MOVIE_COLUMNS,
                        PrivaliaContract.MovieEntry.REGISTRY_TYPE + " = ?",
                        new String[]{PrivaliaContract.SEARCH_REGISTRY_TYPE},
                        null);
            }
            default:
                throw new RuntimeException("Loader Not Implemented: " + id);
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

        mProgressBar.setVisibility(View.INVISIBLE);

        mMovieSearchAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mProgressBar.setVisibility(View.INVISIBLE);
        mMovieSearchAdapter.swapCursor(null);
    }


    @Override
    public void onResume() {
        super.onResume();


    }

    @Override
    public void onPause() {
        if(mSavedInstanceState!= null){
            mSavedInstanceState.putString(SEARCH_TEXT_TAG, mSearchViewText);
        }
        super.onPause();
    }

    @Override
    public void onDestroy() {
        mMovieSearchAdapter.setQueryArg("");
        super.onDestroy();
    }

    @Override
    public void onClick(Movie movie) {
        ((SearchFragment.Callback) getActivity()).OnMovieItemSelected(movie);
    }


}
