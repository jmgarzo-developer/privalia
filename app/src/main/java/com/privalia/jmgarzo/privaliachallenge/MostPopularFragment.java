package com.privalia.jmgarzo.privaliachallenge;


import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.privalia.jmgarzo.privaliachallenge.data.PrivaliaContract;
import com.privalia.jmgarzo.privaliachallenge.model.Movie;
import com.privalia.jmgarzo.privaliachallenge.sync.IntentService.RefreshMovieDataIntentService;
import com.privalia.jmgarzo.privaliachallenge.utilities.DatabaseUtil;
import com.privalia.jmgarzo.privaliachallenge.utilities.DeviceScreenUtils;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * A simple {@link Fragment} subclass.
 */
public class MostPopularFragment extends Fragment implements  LoaderManager.LoaderCallbacks<Cursor>,
        MovieGridViewAdapter.MovieGridViewAdapterOnClickHandler {

    private static final int ID_MOST_POPULAR_LOADER = 34;

    @BindView(R.id.recyclerview_movies_most_popular)
    RecyclerView mRecyclerView;
    @BindView(R.id.tv_error_message_display_most_popular)
    TextView mErrorMenssageDisplay;
    @BindView(R.id.pb_loading_indicator_most_popular)
    ProgressBar mLoadingIndicator;
    @BindView(R.id.swipe_refresh_movie_most_popular)
    SwipeRefreshLayout mSwipeRefreshLayout;

    private MovieGridViewAdapter mGridViewAdapter;

    public interface Callback {
        public void OnMovieItemSelected(Movie movie);
    }


    public MostPopularFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View viewRoot = inflater.inflate(R.layout.fragment_most_popular, container, false);
        ButterKnife.bind(this,viewRoot);
        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), DeviceScreenUtils.getSpanCount(getContext()), LinearLayoutManager.VERTICAL, false);

        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setHasFixedSize(true);

        mGridViewAdapter = new MovieGridViewAdapter(getContext(), this, PrivaliaContract.MOST_POPULAR_REGISTRY_TYPE);
        mRecyclerView.setAdapter(mGridViewAdapter);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshData();
                mSwipeRefreshLayout.setRefreshing(true);
            }
        });

        getActivity().getSupportLoaderManager().initLoader(ID_MOST_POPULAR_LOADER, null, this);

        return viewRoot;
    }

    @Override
    public void onPause() {
        refreshData();
        super.onPause();
    }


    private void refreshData(){
        mGridViewAdapter.setmPageMostPopular(1);
        Intent refreshMovieDataIntent = new Intent(getContext(), RefreshMovieDataIntentService.class);
        refreshMovieDataIntent.putExtra(RefreshMovieDataIntentService.MOVIE_REGISTRY_TYPE_TAG, PrivaliaContract.MOST_POPULAR_REGISTRY_TYPE);
        getContext().startService(refreshMovieDataIntent);
    }


    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        switch (id) {
            case ID_MOST_POPULAR_LOADER: {
                return new CursorLoader(
                        getContext(),
                        PrivaliaContract.MovieEntry.CONTENT_URI,
                        DatabaseUtil.MOVIE_COLUMNS,
                        PrivaliaContract.MovieEntry.REGISTRY_TYPE + " = ?",
                        new String[]{PrivaliaContract.MOST_POPULAR_REGISTRY_TYPE},
                        null);
            }
            default:
                throw new RuntimeException("Loader Not Implemented: " + id);
        }
    }

    private void showMovieThumbs() {
        mErrorMenssageDisplay.setVisibility(View.INVISIBLE);
        mLoadingIndicator.setVisibility(View.INVISIBLE);
        mRecyclerView.setVisibility(View.VISIBLE);
    }

    private void showErrorMessage() {
        mRecyclerView.setVisibility(View.INVISIBLE);
        mLoadingIndicator.setVisibility(View.INVISIBLE);
        mErrorMenssageDisplay.setVisibility(View.VISIBLE);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if(data.getCount()==0){

            showErrorMessage();
        }else{
            showMovieThumbs();
        }
        mGridViewAdapter.swapCursor(data);
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mGridViewAdapter.swapCursor(null);
    }

    @Override
    public void onClick(Movie movie) {
        ((MostPopularFragment.Callback) getActivity()).OnMovieItemSelected(movie);
    }





}
