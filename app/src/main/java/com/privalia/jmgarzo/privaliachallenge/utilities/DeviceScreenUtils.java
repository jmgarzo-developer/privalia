package com.privalia.jmgarzo.privaliachallenge.utilities;

import android.content.Context;

import com.privalia.jmgarzo.privaliachallenge.R;

/**
 * Created by jmgar on 25/01/2018.
 */

public class DeviceScreenUtils {

    public static final int LANDSCAPE_AND_600 = 4;
    public static final int LANDSCAPE_AND_PHONE = 3;
    public static final int PORTRAIT_AND_600=3;
    public static final int DEFAULT = 2;


    public static int getSpanCount(Context context){


        boolean landscape = context.getResources().getBoolean(R.bool.isLandscape);
        boolean width600 = context.getResources().getBoolean(R.bool.is600);
        if(landscape && width600){
            return LANDSCAPE_AND_600;
        }else if(landscape){
            return LANDSCAPE_AND_PHONE;
        }else if(width600){
            return PORTRAIT_AND_600;
        }
        return DEFAULT;
    }
}
