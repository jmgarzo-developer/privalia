package com.privalia.jmgarzo.privaliachallenge.sync.IntentService;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;

import com.privalia.jmgarzo.privaliachallenge.sync.SyncTasks;

/**
 * Created by jmgar on 24/01/2018.
 */

public class MostPopularSyncIntentService extends IntentService {
    public MostPopularSyncIntentService() {

        super("MostPopularSyncIntentService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        SyncTasks.syncMostPopularMovies(this);
    }

}
