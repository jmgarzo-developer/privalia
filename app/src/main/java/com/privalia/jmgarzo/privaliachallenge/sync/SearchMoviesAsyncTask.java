package com.privalia.jmgarzo.privaliachallenge.sync;

import android.content.Context;
import android.os.AsyncTask;

/**
 * Created by jmgar on 26/01/2018.
 */

public class SearchMoviesAsyncTask extends AsyncTask<String,Void,Void> {

    private Context mContext;
   public  SearchMoviesAsyncTask(Context context){
        mContext = context;

    }
    @Override
    protected Void doInBackground(String... params) {
        SyncTasks.syncSearchMovies(mContext, params[0]);

        return null;
    }


}
