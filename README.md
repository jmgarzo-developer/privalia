# privalia challenge

## Synopsis

- Sqlite database to store cache data.
- Instrumented Tests
- Content provider 
- Picasso to load images. 
- firebase jobdispatcher to update cache.
- Rotation control within SearchView

### Contact ###

jmgarzo@gmail.com